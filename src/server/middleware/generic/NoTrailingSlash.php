<?php
    namespace Middleware\generic;

    class NoTrailingSlash{
        function __invoke( $request, $response, $next ){
            $uri = $request->getUri();
            $path = $uri->getPath();

            if( $path != '/' && substr( $path, -1 ) == '/' ){
                $uri = $uri->withPath( substr( $path, 0, -1 ) );
                return $response->withRedirect( (string) $uri, 301 );
            }
            else{
                return $next( $request, $response );
            }
        }
    }
