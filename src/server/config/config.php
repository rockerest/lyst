<?php
    return [
        "settings" => [
            "displayErrorDetails" => true,
            "renderer" => [
                "templates" => __DIR__ . "/../templates/",
                "cache" => false
            ]
        ]
    ];
