<?php
    namespace Config;

    use Middleware\generic\NoTrailingSlash;
    use Slim;

    class Setup{
        function __construct(){}

        private function registerViewHandler( $di ){
            $di[ "view" ] = function ( $container ){
                $settings = $container->get( "settings" )[ "renderer" ];

                $twig = new Slim\Views\Twig( $settings[ "templates" ], [
                    "cache" => false
                ]);

                $twigEnv = $twig->getEnvironment();
                $twigEnv->getExtension( "core" )->setTimezone( "America/Denver" );

                $twig->addExtension( new Slim\Views\TwigExtension(
                    $container[ "router" ],
                    $container[ "request" ]->getUri()
                ) );
                $twig->addExtension( new \Twig_Extension_StringLoader() );

                return $twig;
            };
        }

        private function di( $app ){
            $diContainer = $app->getContainer();

            $this->registerViewHandler( $diContainer );
        }

        private function middleware( $app ){
            $app->add( new NoTrailingSlash() );
        }

        function prepare( $app ){
            $this->di( $app );
            $this->middleware( $app );
        }
    }
