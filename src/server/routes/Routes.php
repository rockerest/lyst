<?php
    namespace Routes;

    class Routes{
        function __construct( $app ){
            $this->app = $app;

            $this->loadRoutes();
        }

        private function loadRoutes(){
            $routes = glob( __DIR__ . "/*.json" );

            foreach( $routes as $file ){
                $fileString = utf8_encode( trim( file_get_contents( $file ) ) );
                $definitions = json_decode( $fileString );

                foreach( $definitions as $rt ){
                    $this->register( $this->app, $rt );
                }
            }
        }

        private function register( $app, $route ){
            $app
                ->get( $route->route, function( $request, $response, $args ) use ( $route ){
                    return $this->view->render(
                        $response,
                        $route->template,
                        [
                            "title" => $route->title
                        ]
                    );
                } )
                ->setName( $route->name );
        }
    }
