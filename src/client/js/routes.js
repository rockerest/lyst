import LystRouter from "routers/lyst";
import AuthenticationRouter from "routers/authentication";
import ErrorRouter from "routers/error";
import Sammy from "sammy";
import _ from "underscore";

var Routes = {};
var router = new Sammy();

window[ window.ns ].router = router;

Routes.start = function(){
    var loc = window.location;
    var startedAt = loc.pathname + loc.search + loc.hash;
    var routers = [
        LystRouter,
        AuthenticationRouter,
        ErrorRouter
    ];
    var count = 0;

    _( routers ).each( function( r ){
        ++count;
        r.register( router );

        if( count === routers.length ){
            router.run( startedAt );
        }
    } );
};

export default Routes;
