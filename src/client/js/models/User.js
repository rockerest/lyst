import Backbone from "strap/backbone";

var UserModel = Backbone.Epoxy.Model.extend( {
    "defaults": {
        "identity": "",
        "firstName": "",
        "middleName": "",
        "lastName": "",
        "nickname": ""
    },

    "computeds": {
        "displayName": {
            "deps": [ "firstName", "nickname", "identity" ],
            "get": function( fname, nick, ident ){
                var display = nick;

                if( !nick ){
                    display = fname || ident;
                }

                return display;
            }
        }
    }
} );

export default UserModel;
