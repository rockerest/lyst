requirejs.config( {
    "paths": {
        // SHORTCUTS
        "vw": "../content/templates/views",
        "lyt": "../content/templates/layouts",
        "translations": "../content/translations",
        "data": "../content/data",
        "strap": "objects/bootstrappers",

        // LIBRARIES
        "backbone": "../../node_modules/backbone/backbone",
        "underscore": "../../node_modules/underscore/underscore",
        "sammy": "../../node_modules/sammy/lib/sammy",
        "jquery": "../../node_modules/jquery/dist/jquery",
        "ribcage": "../../node_modules/backbone-ribcage/build/ribcage",
        "moment": "../../node_modules/moment/min/moment-with-locales",
        "cookies": "../../node_modules/cookies-js/dist/cookies.min",
        "chance": "../../node_modules/chance/chance",

        // LIBRARY PLUGINS
        // Backbone Plugins
        "backbone.epoxy": "../../node_modules/backbone.epoxy/backbone.epoxy",
        // jQuery Plugins
        "qtip2": "../../node_modules/qTip2/dist/jquery.qtip",
        "noty": "../../node_modules/noty/js/noty/packaged/jquery.noty.packaged",
        // Moment Plugins
        "moment-timezone": "../../node_modules/moment-timezone/builds/moment-timezone-with-data-2010-2020",
        // Require plugins
        "text": "../../node_modules/requirejs-text/text",
        "json": "../../node_modules/requirejs-plugins/src/json",
        "i18n": "../../node_modules/i18n/i18n"
    },
    "shim": {
        "backbone": {
            "exports": "Backbone"
        },
        "underscore": {
            "exports": "_"
        },
        "jquery": {
            "exports": "jQuery"
        }
    },
    "map": {
        "*": {
            "moment": "moment-timezone"
        },
        "moment-timezone": {
            "moment": "moment"
        }
    }
} );
