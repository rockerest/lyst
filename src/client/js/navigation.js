import routes from "json!data/routes.json";

export default {
    "to": function( route ){
        var router = window[ window.ns ].router;

        router.setLocation( routes[ route ] || route );
    }
};
