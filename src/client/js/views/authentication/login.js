import Backbone from "strap/backbone";
import vent from "events/authentication/login";

var _ = require( "underscore" );
var tmpl = require( "text!vw/authentication/login.html" );

var AuthenticationLoginView = Backbone.View.extend( {
    "template": _.template( tmpl ),

    "events": {
        'click button[type="submit"]': function( event ){
            vent.trigger( "login", {
                "event": event,
                "identity": this.$( "form input[name='username']" ).val(),
                "password": this.$( "form input[name='password']" ).val()
            } );
        }
    },

    "initialize": function(){
        this.render();
    },

    "render": function(){
        this.$el.html( this.template() );

        return this;
    }
} );

export default AuthenticationLoginView;
