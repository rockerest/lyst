// Libraries
import Backbone from "strap/backbone";
import _ from "underscore";
// Events
import vent from "events/chrome/jumpstart-panel";
// Templates & Translations
import tmpl from "text!vw/chrome/jumpstart-panel.html";

var JumpstartPanelView = Backbone.View.extend( {
    "template": _.template( tmpl ),

    "events": {
        "click #jump-new": ( evt ) => {
            vent.trigger( "list:new" );
            evt.currentTarget.blur();
        }
    },

    "initialize": function(){
        this.render();
    },

    "render": function(){
        this.$el.html( this.template() );

        return this;
    }
} );

export default JumpstartPanelView;
