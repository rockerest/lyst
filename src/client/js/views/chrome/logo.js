import Backbone from "strap/backbone";

var _ = require( "underscore" );
var tmpl = require( "text!vw/chrome/logo.html" );

var LogoView = Backbone.View.extend( {
    "template": _.template( tmpl ),

    "initialize": function(){
        this.render();
    },

    "render": function(){
        this.$el.html( this.template() );

        return this;
    }
} );

export default LogoView;
