import Backbone from "strap/backbone";

var header = Backbone.Epoxy.View.extend( {
    "initialize": function initialize(){
        this.render();
    },

    "render": function render(){
        this.$( "nav" ).html( "Running" );
    }
} );

export default header;
