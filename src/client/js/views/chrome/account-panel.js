import Backbone from "strap/backbone";
import _ from "underscore";
import tmpl from "text!vw/chrome/account-panel.html";

var AccountPanelView = Backbone.Epoxy.View.extend( {
    "template": _.template( tmpl ),

    "bindings": {
        ".displayName": "text:displayName"
    },

    "initialize": function( data ){
        this.model = data.user;

        this.render();
    },

    "render": function(){
        this.$el.html( this.template() );

        return this;
    }
} );

export default AccountPanelView;
