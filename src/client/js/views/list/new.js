// Libraries
import Backbone from "strap/backbone";
import _ from "underscore";
// Templates & Translations
import tmpl from "text!vw/list/new.html";

var NewList = Backbone.View.extend( {
    "template": _.template( tmpl ),

    "initialize": function(){
        this.render();
    },

    "render": function(){
        this.$el.html( this.template() );

        return this;
    }
} );

export default NewList;
