import Backbone from "strap/backbone";
import _ from "underscore";
import tmpl from "text!vw/home/main.html";

var HomeMainView = Backbone.View.extend( {
    "template": _.template( tmpl ),

    "initialize": function(){
        this.render();
    },

    "render": function(){
        this.$el.html( this.template() );

        return this;
    }
} );

export default HomeMainView;
