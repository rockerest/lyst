import Backbone from "strap/backbone";
import _ from "underscore";
import tmpl from "text!vw/error/default.html";

var DefaultErrorView = Backbone.View.extend( {
    "template": _.template( tmpl ),

    "initialize": function( data ){
        this.error = data.error;
        this.route = data.route;
        this.render();
    },

    "render": function(){
        this.$el.html( this.template( { "error": this.error, "route": this.route } ) );
        return this;
    }
} );

export default DefaultErrorView;
