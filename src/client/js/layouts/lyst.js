import tmpl from "text!lyt/lyst.html";

var Ribcage = require( "ribcage" );
var layout = new Ribcage( {
    "regions": {
        "page": "#page"
    },
    "element": "body #content",
    "template": tmpl
} );

export default layout;
