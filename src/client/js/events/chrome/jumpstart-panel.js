import EventFactory from "objects/EventFactory";
import navigate from "navigation";

export default EventFactory.listen( "chromeJumpstartPanel", {
    "list:new": function(){
        navigate.to( "list.new" );
    }
} );
