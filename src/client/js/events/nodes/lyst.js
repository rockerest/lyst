import EventManager from "objects/EventManager";
import LayoutManager from "objects/LayoutManager";
import Security from "objects/Security";
// Home Views
import MainView from "views/home/main";
import PersonalizedMainView from "views/home/personal";
// New List
import NewListView from "views/list/new";

var node = EventManager.listen( "LystNode", {
    "home": function(){
        var layout = LayoutManager.getStandardLayout();
        var isLoggedIn = Security.isUserAuthenticated();
        var view;

        view = isLoggedIn ? PersonalizedMainView : MainView;

        layout.explore( "page" ).show( view );
    },
    "new": function(){
        var layout = LayoutManager.getStandardLayout();

        layout.explore( "page" ).show( NewListView );
    }
} );

export default node;
