import Cookies from "cookies";
import EventFactory from "objects/EventFactory";
import MetaAjax from "ajax/authentication";

var node = EventFactory.listen( "AuthenticationLogin", {
    "login": function( data ){
        data.event.preventDefault();

        MetaAjax
            .authenticate( data.identity, data.password )
            .done( ( auth ) => {
                Cookies.set( "auth", JSON.stringify( auth ) );

                this.broadcast( "LystNode:home" );
            } )
            .fail( function(){
                /* eslint no-console:0 */
                console.log( "The login failed, sorry" );
            } );
    }
} );

export default node;
