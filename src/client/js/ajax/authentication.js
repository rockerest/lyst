import $ from "jquery";
import api from "json!data/api.json";

var AuthenticationAjax = {
    "authenticate": function( identity, password ){
        return $.ajax( {
            "url": api.api + api.authentication.authenticate,
            "type": "get",
            "dataType": "json",
            "data": {
                "identity": identity,
                "password": password
            }
        } );
    }
};

export default AuthenticationAjax;
