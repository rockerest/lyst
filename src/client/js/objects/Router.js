import LayoutManager from "objects/LayoutManager";
import HeaderController from "objects/HeaderController";
import _ from "underscore";

var BaseRouter = function(){
    this.layoutManager = LayoutManager;
    this.header = HeaderController.bindToDom();
};

BaseRouter.prototype.set = function( options ){
    var opts = _( options );

    if( opts.has( "title" ) ){
        this.layoutManager.setLayoutTitle( options.title );
    }
};

BaseRouter.prototype.getName = function(){
    return this.name;
};

export default BaseRouter;
