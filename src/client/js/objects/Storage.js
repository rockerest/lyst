var Storage = {};

Storage.set = function( key, value ){
    window[ window.ns ].storage[ key ] = value;

    return this;
};

Storage.get = function( key ){
    return window[ window.ns ].storage[ key ];
};

Storage.del = function( key ){
    delete window[ window.ns ].storage[ key ];
};

export default Storage;
