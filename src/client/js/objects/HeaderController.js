import Header from "views/chrome/header";

var HeaderController = {
    "bindToDom": function bindToDom(){
        var header = document.getElementById( "header-content" );

        return new Header( {
            "el": header
        } );
    }
};

export default HeaderController;
