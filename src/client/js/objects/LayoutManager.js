import Utilities from "utilities";
import LystLayout from "layouts/lyst";

var appScope = window[ window.ns ];
var LayoutManager = {};
var layout;

function hasLayout(){
    return appScope.layouts.hasManagedLayout;
}

function getLayout(){
    return appScope.layouts.managed;
}

function setLayout( renderedLayout ){
    appScope.layouts.managed = renderedLayout;
    appScope.layouts.hasManagedLayout = true;
}

function clearLayout(){
    appScope.layouts.hasManagedLayout = false;
}

LayoutManager.getStandardLayout = function(){
    layout = this.getLayout( function(){
        LystLayout.render();

        return LystLayout;
    } );

    return layout;
};

LayoutManager.getLayout = function( fallback ){
    var tempLayout;
    var lyt;

    if( hasLayout() ){
        lyt = getLayout();
    }
    else{
        tempLayout = fallback();

        setLayout( tempLayout );

        lyt = tempLayout;
    }

    return lyt;
};

LayoutManager.clearLayout = function(){
    clearLayout();

    return this;
};

LayoutManager.setLayoutTitle = function( name ){
    Utilities.setTitle( name );
};


export default LayoutManager;
