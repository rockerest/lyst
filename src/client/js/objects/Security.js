import Cookies from "cookies";
import UserModel from "models/User";

var Security = {};

Security.isUserAuthenticated = function isUserAuthenticated(){
    var authCookie = Cookies.get( "auth" );
    var token;

    if( authCookie ){
        token = JSON.parse( authCookie );
    }

    return !!token;
};

Security.getAuthenticatedUser = function getAuthenticatedUser(){
    var auth = Cookies.get( "auth" );

    if( auth ){
        auth = JSON.parse( auth );
    }

    return new UserModel( auth );
};

Security.logout = function logout(){
    Cookies.expire( "auth" );

    window[ window.ns ].storage = {};
};

export default Security;
