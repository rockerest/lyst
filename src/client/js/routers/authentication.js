import BaseRouter from "objects/Router";
import Security from "objects/Security";
import LoginView from "views/authentication/login";
import CreateView from "views/authentication/create";

var Mod = function(){};
var AuthenticationRouter;

Mod.prototype = new BaseRouter();

Mod.prototype.register = function( rtr ){
    var self = this;

    rtr.get( /logout\/?/, function(){
        Security.logout();

        window.location.href = "/";
    } );

    rtr.get( /login\/?$/, function(){
        var layout = self.layoutManager.getStandardLayout();

        layout.explore( "page" ).show( LoginView );

        self.set( {
            "title": "Login"
        } );
    } );

    rtr.get( /login\/create\/?/, function(){
        var layout = self.layoutManager.getStandardLayout();

        layout.explore( "page" ).show( CreateView );

        self.set( {
            "title": "Create an Account"
        } );
    } );
};

AuthenticationRouter = new Mod();

export default AuthenticationRouter;
