import BaseRouter from "objects/Router";
import ErrorView from "views/error/default";

var Mod = function(){};
var ErrorRouter;

Mod.prototype = new BaseRouter();

Mod.prototype.register = function( rtr ){
    var self = this;

    rtr.get( /error\/(\d+)\/(.+)$/i, function(){
        var layout = self.layoutManager.getStandardLayout();

        layout.explore( "page" ).show(
            ErrorView,
            {
                "error": this.params.splat[ 0 ],
                "route": this.params.splat[ 1 ]
            }
        );

        self.set( {
            "title": "An Error - " + this.params.splat[ 0 ]
        } );
    } );
};

ErrorRouter = new Mod();

export default ErrorRouter;
