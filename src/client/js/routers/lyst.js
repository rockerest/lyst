import BaseRouter from "objects/Router";
import LystEventNode from "events/nodes/lyst";

var Mod = function(){};
var LystRouter;

Mod.prototype = new BaseRouter();

Mod.prototype.register = function( rtr ){
    var self = this;

    rtr.get( "/", function(){
        LystEventNode.trigger( "home" );

        self.set( {
            "title": "Create, share, rate, and search lists"
        } );
    } );

    rtr.get( /lyst\/new\/?/i, function(){
        LystEventNode.trigger( "new" );

        self.set( {
            "title": "Create a new list"
        } );
    } );
};

LystRouter = new Mod();

export default LystRouter;
