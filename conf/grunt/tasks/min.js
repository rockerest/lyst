module.exports = function( grunt ){
    "use strict";

    grunt.registerTask( "min", "Minify files", function(){
        grunt.task.run( [
            "uglify",
            "cssmin"
        ] );
    } );
};
