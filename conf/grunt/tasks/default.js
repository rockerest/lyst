module.exports = function( grunt ){
    grunt.registerTask( 'default', 'Build lyst', function(){
        grunt.task.run( [
            "copy:content",
            "eslint",
            "babel",
            "requirejs",
            "concat",
            "sass",
            "min"
        ] );
    } );
};
