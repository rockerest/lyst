<?php
    require_once( "vendor/autoload.php" );

    date_default_timezone_set( "America/Denver" );

    $settings = require( "src/server/config/config.php" );

    $app = new Slim\App( $settings );

    $setup = new Config\Setup();
    $setup->prepare( $app );

    $routes = new Routes\Routes( $app );

    $app->run();
